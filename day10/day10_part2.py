from functools import reduce

nb = 256
skip = i = 0
numbers = list(range(nb))

for line in open('input.txt'):
    lengths = [ord(l) for l in line.strip()] + [17, 31, 73, 47, 23]

for _ in range(64):
    for length in lengths:
        sub_list = [numbers[j % nb] for j in range(i, i+length)]
        for j in range(i, i+length):
            numbers[j % nb] = sub_list.pop()

        i = (i + length + skip) % nb
        skip += 1

dense_hash = list()
for i in range(0, nb, 16):
    dense_hash.append("%0.2x" % reduce(lambda a,b: a^b, numbers[i:i+16]))

print('Part 2 : ' + ''.join(dense_hash))
