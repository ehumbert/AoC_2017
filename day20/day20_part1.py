particles = []
for line in open('input.txt'):
    particles.append({vector[0]: [int (c) for c in vector[3:-1].strip().split(',')] for vector in line.strip().split(', ')})

for _ in range(1000):
    for i in range(len(particles)):
        particles[i]['v'][0] += particles[i]['a'][0]
        particles[i]['v'][1] += particles[i]['a'][1]
        particles[i]['v'][2] += particles[i]['a'][2]
        particles[i]['p'][0] += particles[i]['v'][0]
        particles[i]['p'][1] += particles[i]['v'][1]
        particles[i]['p'][2] += particles[i]['v'][2]

print('Part 1 : ' + str(particles.index(min(particles, key=lambda x:sum(map(abs, x['p']))))))
