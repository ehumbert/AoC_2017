paths = []
for line in open('input.txt'):
    paths.append([c for c in line.strip('\n')])

x = paths[0].index('|')
y = 0
direction = 'D'
directions = {'D': (0,1), 'U': (0,-1), 'R': (1,0), 'L': (-1,0)}
cur = '|'
visited = []

steps = 0
while cur != ' ':
    steps += 1
    x += directions[direction][0]
    y += directions[direction][1]
    cur = paths[y][x]

    if cur == '+':
        if direction in ('U', 'D'):
            if paths[y][x-1] != ' ':
                direction = 'L'
            else:
                direction = 'R'
        else:
            if paths[y-1][x] != ' ':
                direction = 'U'
            else:
                direction = 'D'
                
    elif ord(cur) in range(ord('A'), ord('z')):
        visited.append(cur)

print('Part 1 : ' + ''.join(visited))
print('Part 2 : ' + str(steps))
