Advent of Code 2017

Advent of Code is an Advent calendar with new programming puzzles each day between December 1st and December 25th.
If you don't know it yet, I invite you to give it a go at http://adventofcode.com
