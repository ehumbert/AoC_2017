def solve(id_):
    for i in ids[id_]:
        if i not in connected:
            connected.append(i)
            solve(i)

ids = {}
connected = list()
groups = set()

for line in open('input.txt'):
    parse = line.strip().split('<->')
    ids[parse[0].strip()] = [s.strip() for s in parse[1].split(',')]

solve('0')
print('Part 1 : ' + str(len(connected)))

for id_ in ids:
    connected = list()
    solve(id_)
    groups.add(';'.join(sorted(connected)))

print('Part 2 : ' + str(len(groups)))


            

            
