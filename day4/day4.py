import re

nb = 0
nb2 = 0

for line in open('input.txt'):
    words = line.strip().split()
    valid = True
    valid2 = list()
    for i in range(len(words)):
        if re.search(r'\b'+words[i]+r'\b', ' '.join(words[:i]+words[i+1:])) != None:
            valid = False
        valid2 += (False for word in words[:i]+words[i+1:] if sorted(words[i]) == sorted(word))
    nb += valid
    nb2 += len(valid2) < 1
    
print('Part 1 : ' + str(nb))
print('Part 2 : ' + str(nb2))
