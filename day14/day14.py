from functools import reduce
from scipy.ndimage.measurements import label

inp = 'stpzcrnm'
rows = []
nb = 256

def knot_hash(inp, nb):
    lengths = [ord(c) for c in inp] + [17, 31, 73, 47, 23]
    skip = i = 0
    numbers = list(range(nb))
    
    for _ in range(64):
        for length in lengths:
            sub_list = [numbers[j % nb] for j in range(i, i+length)]
            for j in range(i, i+length):
                numbers[j % nb] = sub_list.pop()

            i = (i + length + skip) % nb
            skip += 1

    dense_hash = list()
    for i in range(0, nb, 16):
        dense_hash.append("%0.2x" % reduce(lambda a,b: a^b, numbers[i:i+16]))

    return ''.join(dense_hash)


for row in range(128):
    rows.append(list(map(int, ''.join([bin(int(c, 16))[2:].zfill(4) for c in knot_hash(inp+'-'+str(row), nb)]))))


print('Part 1 : ' + str(sum([sum(row) for row in rows])))
print('Part 2 : ' + str(label(rows)[1]))
        

            

            
