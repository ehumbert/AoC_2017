registers = {}
maxi = 0

for line in open('input.txt'):
    line = [part.strip() for part in line.strip().split('if')]

    action = line[0].split()
    target = "registers['" + action[0] + "']"
    instruction = ' '.join(action[1:]).replace('inc', '+').replace('dec', '-')

    if registers.get(action[0], None) == None:
        registers[action[0]] = 0

    action = line[1].split()
    condition = "registers['"+action[0]+"'] " + ' '.join(action[1:])

    if registers.get(action[0], None) == None:
        registers[action[0]] = 0

    if eval(condition):
        exec(target + ' = ' + target + instruction)

    maxi = max(maxi, max(registers.values()))

print('Part 1 : ' + str(max(registers.values())))
print('Part 2 : ' + str(maxi))
    

