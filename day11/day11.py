import operator

directions = {'nw': (-1, 1, 0), 'n': (0, 1, -1), 'ne': (1, 0, -1),
              'sw': (-1, 0, 1), 's': (0, -1, 1), 'se': (1, -1, 0)}

for line in open('input.txt'):
    steps = line.strip().split(',')
    
    maxi = 0
    cur = (0, 0, 0)
    for step in steps:
        cur = tuple(map(operator.add, cur, directions[step]))
        maxi = max(maxi, max(abs(cur[0]), abs(cur[1]), abs(cur[2])))
        
    print('Part 1 : ' + str(max(abs(cur[0]), abs(cur[1]), abs(cur[2]))))
    print('Part 2 : ' + str(maxi))




            

            
