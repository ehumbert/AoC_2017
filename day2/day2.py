from numpy import amax, amin
from itertools import permutations
    
sum1 = 0
sum2 = list()
for line in open("input.txt"):
    a = [int(i) for i in line.strip().split()]
    sum1 += (amax(a) - amin(a))
    sum2 += (v[0]//v[1] for v in permutations(a, 2) if v[0]%v[1] == 0)

print('Part 1 : ' + str(sum1))
print('Part 2 : '+ str(sum(sum2)))
