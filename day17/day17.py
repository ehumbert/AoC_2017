inp = 345
buffer = [0]

i = 0
for val in range(1, 2018):
    i = (i + inp) % val + 1
    if i == val - 1:
        buffer.append(val)
    else:
        buffer = buffer[:i]+[val]+buffer[i:]
print('Part 1 : ' + str(buffer[buffer.index(2017) + 1]))

i = 0
for val in range(1,50000000+1):
    i = (i + inp) % val + 1
    if i == 1:
        val_one = val
print('Part 2 : ' + str(val_one))
