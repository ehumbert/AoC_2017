def divide(pattern, size):
    if size % 2 == 0:
        nb = 2
    else:
        if size // 3 == 1:
            return [pattern]
        else:
            nb = 3

    squares = []
    pattern_lines = pattern.split('/')
    for line in range(0, size, nb):
        square_line = list(zip(*pattern_lines[line:line+nb]))
        for row in range(0, size, nb):
            squares.append('/'.join([''.join(square_row) for square_row in zip(*square_line[row:row+nb])]))

    return squares

def regroup(squares):
    if len(squares) > 1:
        pattern_lines = [square.split('/') for square in squares]
        nb = len(squares)
        length = int(nb ** 0.5)
        return '/'.join(['/'.join([''.join(row) for row in zip(*pattern_lines[line:line+length])]) for line in range(0, nb, length)])
    else:
        return ''.join(squares)

def flip(pattern):
    return '/'.join([l[::-1] for l in pattern.split('/')])

def rotate(pattern):
    return '/'.join([''.join(t) for t in zip(*pattern.split('/')[::-1])])

def find_rule(pattern):
    cur = pattern
    for angle in range(4):
        if rules.get(cur, False):
            return rules[cur]

        flipped = flip(cur)
        if rules.get(flipped, False):
            return rules[flipped]

        cur = rotate(cur)

rules = {}
for line in open('input.txt'):
    pattern, result = line.strip().split(' => ')
    rules[pattern] = result

pattern = '.#./..#/###'

for i in range(5):
    size = len(pattern.split('/')[0])
    pattern = regroup([find_rule(square) for square in divide(pattern, size)])
    
print('Part 1 : ' + str(pattern.count('#')))

for i in range(13):
    size = len(pattern.split('/')[0])
    pattern = regroup([find_rule(square) for square in divide(pattern, size)])
    
print('Part 2 : ' + str(pattern.count('#')))

