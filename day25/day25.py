class State:
    def __init__(self, z_value, z_shift, z_next, o_value, o_shift, o_next):
        self.z_value = z_value
        self.z_shift = z_shift
        self.z_next = z_next
        self.o_value = o_value
        self.o_shift = o_shift
        self.o_next = o_next

    def step(self, tape, i):
        if tape.get(i, 0) == 0:
            tape[i] = self.z_value
            return i + self.z_shift, self.z_next
        else :
            tape[i] = self.o_value
            return i + self.o_shift, self.o_next
    
tape = {}
i = 0
cur = 'A'

states = {'A': State(1,  1, 'B', 0, -1, 'C'),
          'B': State(1, -1, 'A', 1,  1, 'D'),
          'C': State(1,  1, 'A', 0, -1, 'E'),
          'D': State(1,  1, 'A', 0,  1, 'B'),
          'E': State(1, -1, 'F', 1, -1, 'C'),
          'F': State(1,  1, 'D', 1,  1, 'A')}

for steps in range(12173597):
    i, cur = states[cur].step(tape, i)

print(sum(tape.values()))

    
