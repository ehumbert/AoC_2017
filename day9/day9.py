level = garbage = score = 0
chevron = False

for line in open('input.txt'):
    line = line.strip()

    i = 0
    while i < len(line):
        if line[i] == '!':
            i += 2
            continue
        elif (not chevron) and line[i] == '<':
            chevron = True
        elif chevron and line[i] == '>':
            chevron = False
        elif (not chevron) and line[i] == '{':
            level += 1
        elif (not chevron) and line[i] == '}':
            score += level
            level -= 1
        elif chevron:
            garbage += 1
               
        i += 1

print('Part 1 : ' + str(score))
print('Part 2 : ' + str(garbage))
            
