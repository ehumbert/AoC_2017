history = {}
j = 0

def load_balance(i):
    blocks = banks[i]
    banks[i] = 0
    for _ in range(blocks):
        i = (i + 1) % len(banks)
        banks[i] += 1

for line in open('input.txt'):
    banks = [int(x) for x in line.strip().split()]

while 1:
    load_balance(banks.index(max(banks)))
    j += 1
    try :
        first = history[';'.join(str(banks))]
        break
    except KeyError:
        history[';'.join(str(banks))] = j

print('Part 1 : ' + str(j))
print('Part 2 : ' + str(j - first))
