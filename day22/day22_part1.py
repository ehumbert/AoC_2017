import operator
from collections import defaultdict

nodes = {}
y = -1
for line in open('input.txt'):
    y += 1
    line = line.strip()
    for x in range(len(line)):
        nodes[(x,y)] = True if line[x] == '#' else False

pos = (x//2,y//2)
direction = 'U'
directions = ['L', 'U', 'R', 'D']
move = {'L': (-1,0), 'U': (0,-1), 'R': (1,0), 'D': (0,1)}

def debug_grid():
    x = [node[0] for node in nodes.keys()]
    y = [node[1] for node in nodes.keys()]

    min_x, max_x = min(x), max(x)
    min_y, max_y = min(y), max(y)

    for j in range(min_y, max_y + 1):
        line = ''
        for i in range(min_x, max_x + 1):
            line += '#' if nodes.get((i,j), False) else '.'
        print(line)

infection = 0
for _ in range(10000):
    # Change direction
    cur = nodes.get(pos, False)
    if cur:
        direction = directions[(directions.index(direction) + 1) % len(directions)]
    else:
        direction = directions[(directions.index(direction) - 1) % len(directions)]
        infection += 1

    # Toggle infected state
    nodes[pos] = not cur

    # Move by 1
    pos = tuple(map(operator.add, move[direction], pos))

print(infection)
        
