nb = 256
skip = i = 0
numbers = list(range(nb))

for line in open('input.txt'):
    lengths = [int(l) for l in line.strip().split(',')]

for length in lengths:
    sub_list = [numbers[j % nb] for j in range(i, i+length)]
    for j in range(i, i+length):
        numbers[j % nb] = sub_list.pop()

    i = (i + length + skip) % nb
    skip += 1

print('Part 1 : ' + str(numbers[0] * numbers[1]))




            

            
