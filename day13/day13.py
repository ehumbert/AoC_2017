firewalls = {}

for line in open('input.txt'):
    parse = line.strip().split(': ')
    firewalls[int(parse[0])] = int(parse[1])

def solve(delay):
    severity = 0
    caught = False
    for position in range(max(firewalls.keys()) + 1):
        try:
            if delay % ((firewalls[position] - 1) * 2) < firewalls[position]:
                watchdog = delay % ((firewalls[position] - 1) * 2)
            else:
                watchdog = ((firewalls[position] - 1) * 2) - (delay % ((firewalls[position] - 1) * 2))

            if watchdog == 0:
                severity += position * firewalls[position]
                caught = True

        except KeyError:
            pass

        delay += 1

    return severity, caught

print('Part 1 : ' + str(solve(0)[0]))

delay = 0
while solve(delay)[1]:
    delay += 1
    
print('Part 2 : ' + str(delay))
    


            

            
