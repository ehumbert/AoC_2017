def r_set(registers, i, reg, val):
    registers[reg] = read_value(registers, val)
    return i + 1

def r_sub(registers, i, reg, val):
    registers[reg] -= read_value(registers, val)
    return i + 1

def r_mul(registers, i, reg, val):
    registers[reg] *= read_value(registers, val)
    return i + 1

def r_jnz(registers, i, reg, jump):
    return i + read_value(registers, jump) if read_value(registers, reg) != 0 else i + 1

def read_value(registers, val):
    try:
        return int(val)
    except ValueError:
        return registers[val]


registers = {chr(i): 0 for i in range(ord('a'), ord('h') + 1)}
func = {'set': r_set, 'sub': r_sub, 'mul': r_mul, 'jnz': r_jnz}
instructions = []

for line in open('input.txt'):
    instructions.append(line.strip())

nb_mul = 0
i = 0
while 1:
    try:
        parse = instructions[i].split()
        nb_mul += parse[0] == 'mul'
        i = func[parse[0]](registers, i, parse[1], parse[2])
    except IndexError:
        break

print('Part 1 : ' + str(nb_mul))

h = 0
for x in range((93 * 100) + 100000, (93 * 100) + 100000 + 17000 + 1, 17):
    for i in range(2,x):
        if x % i == 0:
            h += 1
            break

print('Part 2 : ' + str(h))
