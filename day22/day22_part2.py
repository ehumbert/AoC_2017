import operator

nodes = {}
y = -1
for line in open('test.txt'):
    y += 1
    line = line.strip()
    for x in range(len(line)):
        nodes[(x,y)] = 'I' if line[x] == '#' else 'C'

pos = (x//2,y//2)
direction = 'U'
directions = ['L', 'U', 'R', 'D']
move = {'L': (-1,0), 'U': (0,-1), 'R': (1,0), 'D': (0,1)}
state = {'C': 'W', 'W': 'I', 'I': 'F', 'F': 'C'}

def debug_grid():
    x = [node[0] for node in nodes.keys()]
    y = [node[1] for node in nodes.keys()]

    min_x, max_x = min(x), max(x)
    min_y, max_y = min(y), max(y)

    for j in range(min_y, max_y + 1):
        line = ''
        for i in range(min_x, max_x + 1):
            line += '#' if nodes.get((i,j), False) else '.'
        print(line)

infection = 0
for _ in range(10000000):
    # Change direction
    cur = nodes.get(pos, 'C')
    if cur == 'I':
        direction = directions[(directions.index(direction) + 1) % len(directions)]
    elif cur == 'C':
        direction = directions[(directions.index(direction) - 1) % len(directions)]
    elif cur == 'F':
        direction = directions[(directions.index(direction) + 2) % len(directions)]
    else:
        infection += 1

    # Change state
    nodes[pos] = state[cur]

    # Move by 1
    pos = tuple(map(operator.add, move[direction], pos))

print(infection)
        
