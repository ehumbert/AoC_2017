inp = 347991

memory = {}
directions = {'right': (-1,0), 'left': (1,0), 'up': (0,-1), 'down': (0,1)}
j = i = 1
x = y = 0

def go(direction, distance, i, x, y):
    vector = directions.get(direction, (0,0))
    for step in range(distance):
        x += vector[0]
        y += vector[1]
        memory[i] = (x,y)
        i += 1
    return i, x, y

# Initialise the center
i, x, y = go('center', 1, i, x, y)

while i < inp:
    i, x, y = go('right', 1, i, x, y)
    i, x, y = go('up', j, i, x, y)

    #Size of the actual square
    j += 2

    i, x, y = go('left', j-1, i, x, y)
    i, x, y = go('down', j-1, i, x, y)
    i, x, y = go('right', j-1, i, x, y)

print("Part 1 : " + str(sum(map(lambda x:abs(x), memory[inp]))))
    
