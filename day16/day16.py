def spin(programs, position):
    return programs[-position:]+programs[:len(programs)-position]

def exchange(programs, positionA, positionB):
    programs[positionA], programs[positionB] = programs[positionB], programs[positionA]
    return programs

for line in open('input.txt'):
    line = line.strip().split(',')

programs = [chr(i) for i in range(ord('a'), ord('p')+1)]
seen = []
for i in range(1000000):
    s = ''.join(programs)
    if s in seen:
        print('Part 2 : ' + seen[1000000 % i])
        break
    seen.append(s)
    
    for move in line:
        if move[0] == 's':
            programs = spin(programs, int(move[1:]))
        elif move[0] == 'x':
            s = move[1:].split('/')
            programs = exchange(programs, int(s[0]), int(s[1]))
        else:
            s = move[1:].split('/')
            programs = exchange(programs, programs.index(s[0]), programs.index(s[1]))

    if i == 0:
        print('Part 1 : ' + ''.join(programs))
        
