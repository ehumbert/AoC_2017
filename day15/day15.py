class gen:
    def __init__(self, start, factor, final_divider, multiple):
        self.val = start
        self.factor = factor
        self.final_divider = final_divider
        self.multiple = multiple

    def __iter__(self):
        return self

    def next(self):
        self.val = (self.val * self.factor) % self.final_divider
        if self.val % self.multiple == 0:
            return self.val
        else:
            return self.next()
    

start_A = 783
start_B = 325

factor_A = 16807
factor_B = 48271

multiple_A = 4
multiple_B = 8

final_divider = 2147483647

gen_A = gen(start_A, factor_A, final_divider, 1)
gen_B = gen(start_B, factor_B, final_divider, 1)

pairs = 0
for _ in range(40000000):
    pairs += bin(gen_A.next())[2:].zfill(16)[-16:] == bin(gen_B.next())[2:].zfill(16)[-16:]

print('Part 1 : ' + str(pairs))

gen_A = gen(start_A, factor_A, final_divider, multiple_A)
gen_B = gen(start_B, factor_B, final_divider, multiple_B)

pairs = 0
for _ in range(5000000):
    pairs += bin(gen_A.next())[2:].zfill(16)[-16:] == bin(gen_B.next())[2:].zfill(16)[-16:]

print('Part 2 : ' + str(pairs))
    
