def solve(part2):
    jumps = []
    for line in open('input.txt'):
        jumps += (int(x) for x in line.strip().split())
    
    i = j = 0
    while 1:
        try:
            jump = jumps[i]
            jumps[i] += -1 if part2 and jump > 2 else 1
            i += jump
            j += 1
        except IndexError:
            return str(j)

print('Part 1 : ' + solve(False))
print('Part 2 : ' + solve(True))
