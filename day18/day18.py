def r_snd(frequency, registers, i, reg):
    frequency.append(read_register(registers, reg))
    return i + 1

def r_set(registers, i, reg, val):
    registers[reg] = read_value(registers, val)
    return i + 1

def r_add(registers, i, reg, val):
    registers[reg] = read_register(registers, reg) + read_value(registers, val)
    return i + 1

def r_mul(registers, i, reg, val):
    registers[reg] = read_register(registers, reg) * read_value(registers, val)
    return i + 1

def r_mod(registers, i, reg, val):
    registers[reg] = read_register(registers, reg) % read_value(registers, val)
    return i + 1

def r_rcv(frequency, registers, i, reg):
    if read_register(registers, reg) != 0:
        print('Part 1 : ' + str(frequency.pop()))
    return i + 9999

def r_jgz(registers, i, reg, jump):
    return i + read_value(registers, jump) if registers[reg] > 0 else i + 1

def read_register(registers, reg):
    try:
        return registers[reg]
    except KeyError:
        registers[reg] = 0
        return registers[reg]

def read_value(registers, val):
    try:
        return int(val)
    except ValueError:
        return read_register(registers, val)


registers = {}
func = {'snd': r_snd, 'set': r_set, 'add': r_add, 'mul': r_mul, 'mod': r_mod, 'rcv': r_rcv, 'jgz': r_jgz}
instructions = {}
frequency = []

i = 0
for line in open('input.txt'):
    instructions[i] = line.strip()
    i += 1

i = 0
while 1:
    try:
        parse = instructions[i].split()
        if parse[0] in ['snd', 'rcv']:
            i = func[parse[0]](frequency, registers, i, parse[1])
        else:
            i = func[parse[0]](registers, i, parse[1], parse[2])
    except KeyError:
        break
        
