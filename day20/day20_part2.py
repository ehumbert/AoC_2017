from collections import defaultdict

particles = []
for line in open('input.txt'):
    particles.append({vector[0]: [int (c) for c in vector[3:-1].strip().split(',')] for vector in line.strip().split(', ')})

for _ in range(1000):
    seen = defaultdict(list)
    for particle in particles:
        particle['v'][0] += particle['a'][0]
        particle['v'][1] += particle['a'][1]
        particle['v'][2] += particle['a'][2]
        particle['p'][0] += particle['v'][0]
        particle['p'][1] += particle['v'][1]
        particle['p'][2] += particle['v'][2]

        seen[','.join([str(p) for p in particle['p']])].append(particle)

    for collide in seen.values():
        if len(collide) > 1:
            for collision in collide:
                particles.remove(collision)

print('Part 2 : ' + str(len(particles)))
