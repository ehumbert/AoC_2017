def r_snd(frequency, registers, i, reg):
    frequency.append(read_register(registers, reg))
    return i + 1

def r_set(registers, i, reg, val):
    registers[reg] = read_value(registers, val)
    return i + 1

def r_add(registers, i, reg, val):
    registers[reg] = read_register(registers, reg) + read_value(registers, val)
    return i + 1

def r_mul(registers, i, reg, val):
    registers[reg] = read_register(registers, reg) * read_value(registers, val)
    return i + 1

def r_mod(registers, i, reg, val):
    registers[reg] = read_register(registers, reg) % read_value(registers, val)
    return i + 1

def r_rcv(frequency, registers, i, reg):
    registers[reg] = frequency.pop(0)
    return i + 1

def r_jgz(registers, i, reg, jump):
    return i + read_value(registers, jump) if read_value(registers, reg) > 0 else i + 1

def read_register(registers, reg):
    try:
        return registers[reg]
    except KeyError:
        registers[reg] = 0
        return registers[reg]

def read_value(registers, val):
    try:
        return int(val)
    except ValueError:
        return read_register(registers, val)


instructions = {}
func = {'snd': r_snd, 'set': r_set, 'add': r_add, 'mul': r_mul, 'mod': r_mod, 'rcv': r_rcv, 'jgz': r_jgz}

i = 0
for line in open('input.txt'):
    instructions[i] = line.strip()
    i += 1

progs = [{}, {'p': 1}]
data = [[], []]
pos = [0, 0]
status = [1, 1] # 0 = done, 1 = running, 2 = waiting
cur = 0

tot = 0
while 1:
    try:
        parse = instructions[pos[cur]].split()
        if parse[0] == 'snd':
            pos[cur] = func[parse[0]](data[cur], progs[cur], pos[cur], parse[1])
            tot += cur
        elif parse[0] == 'rcv':
            if data[1-cur]:
                status[cur] = 1
                pos[cur] = func[parse[0]](data[1-cur], progs[cur], pos[cur], parse[1])
            elif (not status[1-cur]) or (len(data[cur]) == 0 and status[1-cur] == 2):
                break
            else:
                status[cur] = 2
                cur = 1 - cur
        else:
            pos[cur] = func[parse[0]](progs[cur], pos[cur], parse[1], parse[2])
    except KeyError:
        if not status[1-cur]:
            break
        status[cur] = 0
        cur = 1 - cur

print(tot)


        
