from threading import Thread, Semaphore
from queue import Queue

class prog(Thread):
    def __init__(self, start, instructions, lock_in, lock_out, in_q, out_q):
        Thread.__init__(self)
        self.out = []
        self.registers = {}
        self.registers['p'] = start
        self.default = 0
        self.i = 0
        self.count = 0
        self.instructions = instructions
        self.lock_in = lock_in
        self.lock_out = lock_out
        self.in_q = in_q
        self.out_q = out_q
        self.func = {'snd': self.r_snd, 'set': self.r_set, 'add': self.r_add,
                     'mul': self.r_mul, 'mod': self.r_mod, 'rcv': self.r_rcv,
                     'jgz': self.r_jgz}

    def r_snd(self, reg):
        self.out_q.put(self.read_register(reg))
        self.lock_out.release()
        self.count += 1
        self.i += 1

    def r_set(self, reg, val):
        self.registers[reg] = self.read_value(val)
        self.i += 1

    def r_add(self, reg, val):
        self.registers[reg] = self.read_register(reg) + self.read_value(val)
        self.i += 1

    def r_mul(self, reg, val):
        self.registers[reg] = self.read_register(reg) * self.read_value(val)
        self.i += 1

    def r_mod(self, reg, val):
        self.registers[reg] = self.read_register(reg) % self.read_value(val)
        self.i += 1

    def r_rcv(self, reg):
        s = 0
        if self.lock_in.acquire(timeout=0.5):
            self.registers[reg] = self.in_q.get()
            self.i += 1
        else:
            self.i = None

    def r_jgz(self, reg, jump):
        self.i += self.read_value(jump) if self.read_register(reg) > 0 else 1

    def read_register(self, reg):
        try:
            return self.registers[reg]
        except KeyError:
            self.registers[reg] = self.default
            return self.registers[reg]

    def read_value(self, val):
        try:
            return int(val)
        except ValueError:
            return self.read_register(val)

    def parse(self, instruction):
        parsed = instruction.split()
        if parsed[0] in ['snd', 'rcv']:
            self.func[parsed[0]](parsed[1])
        else:
            self.func[parsed[0]](parsed[1], parsed[2])

    def run(self):
        while 1:
            try:
                self.parse(self.instructions[self.i])
            except KeyError:
                break
        return
        

if __name__ == '__main__':
    instructions = {}

    i = 0
    for line in open('input.txt'):
        instructions[i] = line.strip()
        i += 1

    lock0 = Semaphore(value=0)
    lock1 = Semaphore(value=0)

    q_0 = Queue()
    q_1 = Queue()
    
    prog0 = prog(0, instructions, lock0, lock1, q_0, q_1)
    prog1 = prog(1, instructions, lock1, lock0, q_1, q_0)

    prog0.start()
    prog1.start()

    prog1.join()
    prog0.join()

    print(prog1.count)
    
