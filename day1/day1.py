for line in open("input.txt"):
    line = line.strip()
    print('Part 1 : ', sum([int(line[i]) for i in range(len(line)) if line[i] == line[(i+1)%len(line)]]))
    print('Part 2 : ', sum([int(line[i]) for i in range(len(line)) if line[i] == line[(i+(len(line)//2))%len(line)]]))
