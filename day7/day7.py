import collections

class Prog:
    def __init__(self, name, weight, sub_progs):
        self.name = name
        self.weight = weight
        self.sub_progs = sub_progs

    def get_weight(self, programs):
        return self.weight + sum([programs[sp].get_weight(programs) for sp in self.sub_progs])

    def __repr__(self):
        return self.name+' ('+str(self.weight)+') -> '+', '.join(self.sub_progs)+'\n'

def balance(current):
    loads = [programs[sp].get_weight(programs) for sp in programs[current].sub_progs]
    unbalance = collections.Counter(loads).most_common()[-1]
    return current if unbalance[1] > 1 else balance(programs[current].sub_progs[loads.index(unbalance[0])])

programs = {}
bottom = set()
top = set()

for line in open('input.txt'):
    line = line.strip().split()
    name = line[0]
    weight = int(line[1][1:-1])
    sub_progs = [prog.strip(',') for prog in line[3:]] if '->' in line else list()
    programs[name] = Prog(name, weight, sub_progs)

for prog in programs.values():
    if len(prog.sub_progs) > 0:
        bottom.add(prog.name)
        for sp in prog.sub_progs:
            top.add(sp)

first = (bottom-top).pop()
print('Part 1 : ' + first)

unbalanced = balance(first)
for prog in programs.values():
    if unbalanced in prog.sub_progs:
        unbalance = collections.Counter([programs[sp].get_weight(programs) for sp in programs[prog.name].sub_progs]).most_common()
        print('Part 2 : ' + str(programs[unbalanced].weight - abs(unbalance[0][0] - unbalance[-1][0])))
        break
