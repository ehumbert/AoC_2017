from copy import deepcopy

def find_all(components, bridge, start, all_path):
    nexts = find_nexts(components, start)
    if not nexts:
        return '--'.join(bridge)

    for next_component in nexts:
        # Current Bridge
        next_bridge = deepcopy(bridge)
        next_bridge.append('/'.join(next_component))

        # Next start
        next_start = next_component[1 - next_component.index(start)]

        # Remove next component
        next_components = deepcopy(components)
        next_components.remove(next_component)

        cur = find_all(next_components, next_bridge, next_start, all_path)
        if isinstance(cur, str) and cur not in all_path:
            all_path.append(cur)
    
    return all_path

def find_nexts(components, start):
    possible = []
    for component in components:
        if start in component:
            possible.append(component)

    return possible

components = []
for line in open('input.txt'):
    components.append(line.strip().split('/'))

path = find_all(components, [], '0', [])
weighted_path = [[sum(map(int, piece)) for piece in map(lambda s: s.split('/'), component)] for component in map(lambda s: s.split('--'), path)]
max_len = max(map(len, weighted_path))

print('Part 1 ' + str(max(map(sum, weighted_path))))
print('Part 2 ' + str(max(map(sum, filter(lambda x: len(x) == max_len, weighted_path)))))



